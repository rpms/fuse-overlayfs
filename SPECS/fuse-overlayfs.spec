%global git0 https://github.com/containers/%{name}
%global commit0 6d269aa28e3fe78c117fecb5e2cb8e1e231fcb10
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

Name: fuse-overlayfs
Version: 0.3
Release: 5%{?dist}
Summary: FUSE overlay+shiftfs implementation for rootless containers
License: GPLv3+
URL: %{git0}
# no go-md2man in ix86
ExcludeArch: %{ix86} ppc64
Source0: %{git0}/archive/%{commit0}/%{name}-%{shortcommit0}.tar.gz
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: fuse3-devel
BuildRequires: gcc
BuildRequires: git
BuildRequires: go-md2man
BuildRequires: make
Provides: bundled(gnulib) = cb634d40c7b9bbf33fa5198d2e27fdab4c0bf143

%description
%{summary}.

%package devel
Summary: %{summary}
BuildArch: noarch

%description devel
%{summary}

This package contains library source intended for
building other packages which use import path with
%{import_path} prefix.

%prep
%autosetup -Sgit -n %{name}-%{commit0}

%build
./autogen.sh
./configure --prefix=%{_usr} --libdir=%{_libdir}
%{__make} generate-man

%install
make DESTDIR=%{buildroot} install install-man

%check

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files
%license COPYING
%doc README.md
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1.gz

%changelog
* Mon Aug 19 2019 Jindrich Novy <jnovy@redhat.com> - 0.3-5
- revert fuse-overlayfs to commit 6d269aa
- Resolves: #1720707

* Wed Feb 20 2019 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.3-4.dev.gitd760789
- rebase

* Wed Jan 16 2019 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.3-2
- rebase
- Resolves:#1666510

* Wed Oct 17 2018 Lokesh Mandvekar <lsm5@fedoraproject.org> - 0.1-7.dev.git50c7a50
- Resolves: #1640232
- built commit 50c7a50

* Fri Aug 10 2018 Lokesh Mandvekar <lsm5@fedoraproject.org> - 0.1-6.dev.git1c72a1a
- Resolves: #1614856 - add manpage
- built commit 1c72a1a
- add BR: go-md2man

* Fri Aug 10 2018 Lokesh Mandvekar <lsm5@fedoraproject.org> - 0.1-5.dev.gitd40ac75
- built commit d40ac75
- remove fedora bz ids
- Exclude ix86 and ppc64

* Mon Jul 30 2018 Lokesh Mandvekar <lsm5@fedoraproject.org> - 0.1-4.dev.git79c70fd
- Resolves: #1609598 - initial upload to Fedora
- bundled gnulib

* Mon Jul 30 2018 Lokesh Mandvekar <lsm5@fedoraproject.org> - 0.1-3.dev.git79c70fd
- correct license field

* Mon Jul 30 2018 Lokesh Mandvekar <lsm5@fedoraproject.org> - 0.1-2.dev.git79c70fd
- fix license

* Sun Jul 29 2018 Lokesh Mandvekar <lsm5@fedoraproject.org> - 0.1-1.dev.git13575b6
- First package for Fedora
